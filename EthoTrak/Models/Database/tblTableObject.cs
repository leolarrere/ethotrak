using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class tblTableObject {
        public tblTableObject() { }
        public int ID { get; set; }
        public string TableName { get; set; }
        public string Description { get; set; }
        public string DatabaseName { get; set; }
        public string SourceTable { get; set; }
        public int? TableType { get; set; }
    }
}
