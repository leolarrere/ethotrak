using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblBEHAV_ID {
        public TblBEHAV_ID() { }
        public virtual int BEHAV_ID { get; set; }
        public virtual int? MSTR_ID { get; set; }
        public virtual int? SPEC_ID { get; set; }
        public virtual string BEHAV { get; set; }
        public virtual string DEFIN { get; set; }
        public virtual System.Nullable<bool> ALL_OCC { get; set; }
        public virtual System.DateTime? ADD_DATE { get; set; }
        public virtual int? BEH_PRNT { get; set; }
        public virtual System.Nullable<bool> Locked { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
