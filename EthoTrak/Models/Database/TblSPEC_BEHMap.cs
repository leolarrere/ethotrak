using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblSPEC_BEHMap : ClassMap<TblSPEC_BEH> {
        
        public TblSPEC_BEHMap() {
			Table("TblSPEC_BEH");
			LazyLoad();
			CompositeId();
			Map(x => x.SPEC_ID).Column("SPEC_ID");
			Map(x => x.BEHAV_ID).Column("BEHAV_ID");
        }
    }
}
