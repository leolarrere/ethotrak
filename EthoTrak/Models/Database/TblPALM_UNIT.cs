using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblPALM_UNIT {
        public TblPALM_UNIT() { }
        public int PALM_NO { get; set; }
        public string PALM_ID { get; set; }
        public string SER_NO { get; set; }
        public string MODEL { get; set; }
        public System.Nullable<bool> USB { get; set; }
        public string OS { get; set; }
        public string NOTES { get; set; }
    }
}
