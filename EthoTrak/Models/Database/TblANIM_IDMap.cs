using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using FluentValidation;

namespace EthoTrak.DatabaseModels {
  
    
    public class TblANIM_IDMap : ClassMap<TblANIM_ID> {
        
        public TblANIM_IDMap() {
			Table("TblANIM_ID");
			LazyLoad();
			Id(x => x.ANIM_ID).GeneratedBy.Identity().Column("ANIM_ID");
			Map(x => x.SPEC_ID).Column("SPEC_ID");
			Map(x => x.INST_ID).Column("INST_ID").Length(10);
			Map(x => x.DESC_ANIM).Column("DESC_ANIM").Length(50);
			Map(x => x.SEX).Column("SEX").Length(5);
			Map(x => x.STATUS).Column("STATUS").Length(1);
			Map(x => x.COMMENTS).Column("COMMENTS").Length(255);
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
