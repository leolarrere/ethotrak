using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class tblsAction {
        public tblsAction() { }
        public int ID { get; set; }
        public int? ActionCode { get; set; }
        public string ActionName { get; set; }
    }
}
