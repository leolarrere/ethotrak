using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class sysdiagram {
        public sysdiagram() { }
        public int diagram_id { get; set; }
        public string name { get; set; }
        public int principal_id { get; set; }
        public int? version { get; set; }
        public byte[] definition { get; set; }
    }
}
