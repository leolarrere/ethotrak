using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblPALM_LOANMap : ClassMap<TblPALM_LOAN> {
        
        public TblPALM_LOANMap() {
			Table("TblPALM_LOAN");
			LazyLoad();
			Id(x => x.LOAN_NO).GeneratedBy.Identity().Column("LOAN_NO");
			Map(x => x.PALM_NO).Column("PALM_NO");
			Map(x => x.EXHIB_ID).Column("EXHIB_ID");
			Map(x => x.KEEP_ID).Column("KEEP_ID");
			Map(x => x.LOAN).Column("LOAN");
			Map(x => x.RETURN).Column("RETURN");
			Map(x => x.COMMENTS).Column("COMMENTS").Length(255);
			Map(x => x.CONT_INFO).Column("CONT_INFO").Length(50);
        }
    }
}
