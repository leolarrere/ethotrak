using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class tbldErrorLogMap : ClassMap<tbldErrorLog> {
        
        public tbldErrorLogMap() {
			Table("tbldErrorLogs");
			LazyLoad();
			Id(x => x.ID).GeneratedBy.Identity().Column("ID");
			Map(x => x.CustomNumber).Column("CustomNumber");
			Map(x => x.ErrorNumber).Column("ErrorNumber");
			Map(x => x.ErrorDescription).Column("ErrorDescription").Length(1073741823);
			Map(x => x.DateLog).Column("DateLog");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
