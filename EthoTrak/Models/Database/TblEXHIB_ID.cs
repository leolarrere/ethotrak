using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblEXHIB_ID {
        public TblEXHIB_ID() { }
        public virtual int EXHIB_ID { get; set; }
        public virtual string EXHIB { get; set; }
        public virtual System.DateTime? ADD_DATE { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime?ACT_DT { get; set; }
    }
}
