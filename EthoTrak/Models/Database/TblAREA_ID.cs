using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblAREA_ID {
        public TblAREA_ID() { }
        public virtual int AREA_ID { get; set; }
        public virtual int? EXHIB_ID { get; set; }
        public virtual string AREA { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
