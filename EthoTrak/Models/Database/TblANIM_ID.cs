using System;
using System.Text;
using System.Collections.Generic;
using FluentValidation;


namespace EthoTrak.DatabaseModels
{

    [FluentValidation.Attributes.Validator(typeof(AnimalValidator))]
    public class TblANIM_ID
    {
        public TblANIM_ID() { }
        public virtual int ANIM_ID { get; set; }
        public virtual int? SPEC_ID { get; set; }
        public virtual string INST_ID { get; set; }
        public virtual string DESC_ANIM { get; set; }
        public virtual string SEX { get; set; }
        public virtual string STATUS { get; set; }
        public virtual string COMMENTS { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }


    public class AnimalValidator : AbstractValidator<TblANIM_ID>
    {
        public AnimalValidator()
        {
            RuleFor(model => model.DESC_ANIM)
                .NotEmpty()
                .WithMessage("Description is required.");
            //.Length(1, 256);

            RuleFor(model => model.INST_ID)
                .NotEmpty()
                .WithMessage("Institution name is required.");

            RuleFor(model => model.SEX)
    .NotEmpty()
    .WithMessage("Sex is required.");

            RuleFor(model => model.STATUS)
    .NotEmpty()
    .WithMessage("Status is required.");
        }
    }


}
