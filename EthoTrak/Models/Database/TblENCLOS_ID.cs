using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblENCLOS_ID {
        public TblENCLOS_ID() { }
        public virtual int ENCLOS_ID { get; set; }
        public virtual int? AREA_ID { get; set; }
        public virtual string ENCLOS { get; set; }
        public virtual System.Nullable<bool> AVAIL { get; set; }
        public virtual string COMMENTS { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
