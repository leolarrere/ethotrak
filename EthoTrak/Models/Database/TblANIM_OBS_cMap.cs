using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblANIM_OBS_cMap : ClassMap<TblANIM_OBS_c> {
        
        public TblANIM_OBS_cMap() {
			Table("TblANIM_OBS_c");
			LazyLoad();
			//CompositeId().KeyProperty(x => x.USER_ID, "USER_ID").KeyProperty(x => x.SES_NO, "SES_NO").KeyProperty(x => x.OBS_NO, "OBS_NO");
			//Map(x => x.ID).Column("ID").Not.Nullable();
            Id(x => x.ID).GeneratedBy.Identity().Column("ID");
            Map(x => x.USER_ID).Column("USER_ID");
            Map(x => x.SES_NO).Column("SES_NO");
            Map(x => x.OBS_NO).Column("OBS_NO");
			Map(x => x.SPEC_ID).Column("SPEC_ID");
			Map(x => x.ID_OBS).Column("ID_OBS");
			Map(x => x.ENCLOS_ID).Column("ENCLOS_ID");
			Map(x => x.BEHAV_ID).Column("BEHAV_ID");
			Map(x => x.LOC_ID).Column("LOC_ID");
			Map(x => x.DATE_OBS).Column("DATE_OBS");
			Map(x => x.TIME_OBS).Column("TIME_OBS");
			Map(x => x.NEIGH).Column("NEIGH");
			Map(x => x.COMMENTS).Column("COMMENTS").Length(255);
			Map(x => x.FLAG).Column("FLAG");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
