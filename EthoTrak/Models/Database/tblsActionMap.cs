using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class tblsActionMap : ClassMap<tblsAction> {
        
        public tblsActionMap() {
			Table("tblsActions");
			LazyLoad();
			Id(x => x.ID).GeneratedBy.Identity().Column("ID");
			Map(x => x.ActionCode).Column("ActionCode");
			Map(x => x.ActionName).Column("ActionName").Length(50);
        }
    }
}
