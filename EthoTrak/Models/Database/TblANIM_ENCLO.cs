using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblANIM_ENCLO {
        public TblANIM_ENCLO() { }
        public virtual int ENCLOS_REC { get; set; }
        public virtual int? ANIM_ID { get; set; }
        public virtual int? ENCLOS_ID { get; set; }
        public virtual System.DateTime? IN_DATE { get; set; }
        public virtual System.DateTime? OUT_DATE { get; set; }
        public virtual string COMMENTS { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
