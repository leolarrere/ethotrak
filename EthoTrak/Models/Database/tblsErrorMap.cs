using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class tblsErrorMap : ClassMap<tblsError> {
        
        public tblsErrorMap() {
			Table("tblsErrors");
			LazyLoad();
			Id(x => x.ID).GeneratedBy.Identity().Column("ID");
			Map(x => x.ErrorNumber).Column("ErrorNumber");
			Map(x => x.ErrorDescripton).Column("ErrorDescripton").Length(50);
			Map(x => x.ErrorMessage).Column("ErrorMessage").Length(255);
			Map(x => x.DateAdded).Column("DateAdded");
			Map(x => x.DateUpdated).Column("DateUpdated");
        }
    }
}
