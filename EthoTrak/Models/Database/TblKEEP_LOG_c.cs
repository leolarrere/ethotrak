using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblKEEP_LOG_c {
        public TblKEEP_LOG_c() { }
        public string USER_ID { get; set; }
        public int SES_NO { get; set; }
        public int ID { get; set; }
        public int? KEEP_ID { get; set; }
        public int? AREA_ID { get; set; }
        public string SKY { get; set; }
        public int? TEMP_LOG { get; set; }
        public int? HUMI_LOG { get; set; }
        public string CROWD { get; set; }
        public System.DateTime?DATE_IN { get; set; }
        public System.DateTime?TIME_IN { get; set; }
        public System.DateTime?DATE_OUT { get; set; }
        public System.DateTime?TIME_OUT { get; set; }
        public int? SES_SC { get; set; }
        public int? ACT_TP { get; set; }
        public System.DateTime?ACT_DT { get; set; }
        public int? TYPE { get; set; }
        public string COMMENTS { get; set; }
    }
}
