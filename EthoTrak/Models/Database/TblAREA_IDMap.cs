using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblAREA_IDMap : ClassMap<TblAREA_ID> {
        
        public TblAREA_IDMap() {
			Table("TblAREA_ID");
			LazyLoad();
			Id(x => x.AREA_ID).GeneratedBy.Identity().Column("AREA_ID");
			Map(x => x.EXHIB_ID).Column("EXHIB_ID");
            Map(x => x.AREA).Column("AREA").Length(50);
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
