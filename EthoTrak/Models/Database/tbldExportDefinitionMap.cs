using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class tbldExportDefinitionMap : ClassMap<tbldExportDefinition> {
        
        public tbldExportDefinitionMap() {
			Table("tbldExportDefinition");
			LazyLoad();
			CompositeId();
			Map(x => x.ExportID).Column("ExportID").Length(50);
			Map(x => x.ExportName).Column("ExportName").Length(50);
			Map(x => x.ExportDesc).Column("ExportDesc").Length(250);
			Map(x => x.Source).Column("Source").Length(255);
			Map(x => x.SourceFilter).Column("SourceFilter").Length(255);
			Map(x => x.RollBackFilter).Column("RollBackFilter").Length(50);
			Map(x => x.AffectedTable).Column("AffectedTable").Length(50);
			Map(x => x.PrimaryIDName).Column("PrimaryIDName").Length(50);
			Map(x => x.AffectedColumns).Column("AffectedColumns").Length(1073741823);
			Map(x => x.ActionCode).Column("ActionCode");
        }
    }
}
