using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblKEEP_IDMap : ClassMap<TblKEEP_ID> {
        
        public TblKEEP_IDMap() {
			Table("TblKEEP_ID");
			LazyLoad();
			Id(x => x.KEEP_ID).GeneratedBy.Identity().Column("KEEP_ID");
			Map(x => x.FIRST).Column("FIRST").Length(50);
			Map(x => x.MID_INIT).Column("MID_INIT").Length(1);
			Map(x => x.LAST).Column("LAST").Length(50);
			Map(x => x.KEEP_INIT).Column("KEEP_INIT").Length(50);
			Map(x => x.ADD_DATE).Column("ADD_DATE");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
