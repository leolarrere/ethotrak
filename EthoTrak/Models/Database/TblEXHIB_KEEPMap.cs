using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblEXHIB_KEEPMap : ClassMap<TblEXHIB_KEEP> {
        
        public TblEXHIB_KEEPMap() {
			Table("TblEXHIB_KEEP");
			LazyLoad();
			Id(x => x.REC_ID).GeneratedBy.Identity().Column("REC_ID");
			Map(x => x.KEEP_ID).Column("KEEP_ID");
			Map(x => x.EXHIB_ID).Column("EXHIB_ID");
			Map(x => x.ADD_DATE).Column("ADD_DATE");
			Map(x => x.REM_DATE).Column("REM_DATE");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
