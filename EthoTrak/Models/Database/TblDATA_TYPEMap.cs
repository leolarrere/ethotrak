using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblDATA_TYPEMap : ClassMap<TblDATA_TYPE> {
        
        public TblDATA_TYPEMap() {
			Table("TblDATA_TYPE");
			LazyLoad();
			Id(x => x.ID).GeneratedBy.Identity().Column("ID");
			Map(x => x.TYPE_ID).Column("TYPE_ID");
			Map(x => x.TYPE_NAME).Column("TYPE_NAME").Length(50);
			Map(x => x.TYPE_DEF).Column("TYPE_DEF").Length(255);
        }
    }
}
