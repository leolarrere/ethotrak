using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblEXHIB_KEEP {
        public TblEXHIB_KEEP() { }
        public virtual int REC_ID { get; set; }
        public virtual int? KEEP_ID { get; set; }
        public virtual int? EXHIB_ID { get; set; }
        public virtual System.DateTime? ADD_DATE { get; set; }
        public virtual System.DateTime? REM_DATE { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
