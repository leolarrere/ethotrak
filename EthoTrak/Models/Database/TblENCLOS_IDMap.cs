using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblENCLOS_IDMap : ClassMap<TblENCLOS_ID> {
        
        public TblENCLOS_IDMap() {
			Table("TblENCLOS_ID");
			LazyLoad();
			Id(x => x.ENCLOS_ID).GeneratedBy.Identity().Column("ENCLOS_ID");
			Map(x => x.AREA_ID).Column("AREA_ID");
			Map(x => x.ENCLOS).Column("ENCLOS").Length(50);
			Map(x => x.AVAIL).Column("AVAIL");
			Map(x => x.COMMENTS).Column("COMMENTS").Length(200);
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
