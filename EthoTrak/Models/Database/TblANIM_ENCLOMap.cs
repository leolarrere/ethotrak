using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblANIM_ENCLOMap : ClassMap<TblANIM_ENCLO> {
        
        public TblANIM_ENCLOMap() {
			Table("TblANIM_ENCLOS");
			LazyLoad();
			Id(x => x.ENCLOS_REC).GeneratedBy.Identity().Column("ENCLOS_REC");
			Map(x => x.ANIM_ID).Column("ANIM_ID");
			Map(x => x.ENCLOS_ID).Column("ENCLOS_ID");
			Map(x => x.IN_DATE).Column("IN_DATE");
			Map(x => x.OUT_DATE).Column("OUT_DATE");
			Map(x => x.COMMENTS).Column("COMMENTS").Length(50);
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
