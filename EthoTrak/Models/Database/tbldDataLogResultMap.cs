using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class tbldDataLogResultMap : ClassMap<tbldDataLogResult> {
        
        public tbldDataLogResultMap() {
			Table("tbldDataLogResult");
			LazyLoad();
			CompositeId();
			Map(x => x.SourceID).Column("SourceID");
			Map(x => x.DateUploaded).Column("DateUploaded");
        }
    }
}
