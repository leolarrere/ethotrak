using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblDEVICE {
        public TblDEVICE() { }
        public int DevID { get; set; }
        public string USER_ID { get; set; }
        public System.DateTime?DateCreated { get; set; }
    }
}
