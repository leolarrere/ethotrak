using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblSPEC_BEH {
        public TblSPEC_BEH() { }
        public int? SPEC_ID { get; set; }
        public int? BEHAV_ID { get; set; }
    }
}
