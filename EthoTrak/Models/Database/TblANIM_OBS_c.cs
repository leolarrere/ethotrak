using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblANIM_OBS_c {
        public TblANIM_OBS_c() { }
        public virtual string USER_ID { get; set; }
        public virtual int SES_NO { get; set; }
        public virtual int OBS_NO { get; set; }
        public virtual int ID { get; set; }
        public virtual int? SPEC_ID { get; set; }
        public virtual int? ID_OBS { get; set; }
        public virtual int? ENCLOS_ID { get; set; }
        public virtual int? BEHAV_ID { get; set; }
        public virtual int? LOC_ID { get; set; }
        public virtual System.DateTime? DATE_OBS { get; set; }
        public virtual System.DateTime? TIME_OBS { get; set; }
        public virtual System.Nullable<bool> NEIGH { get; set; }
        public virtual string COMMENTS { get; set; }
        public virtual System.Nullable<bool> FLAG { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }

    public class TblANIM_OBS_c_ViewModel
    {
        public TblANIM_OBS_c_ViewModel() { }
        public string USER_ID { get; set; }
        public int SES_NO { get; set; }
        public int OBS_NO { get; set; }
        public int ID { get; set; }
        public string SPEC { get; set; }
        public string ANIMAL { get; set; }
        public string ENCLOSURE { get; set; }
        public string BEHAVIOR { get; set; }
        public string LOCATION { get; set; }
        public System.DateTime? DATE_OBS { get; set; }
        public System.DateTime? TIME_OBS { get; set; }
        public System.Nullable<bool> NEIGH { get; set; }
        public string COMMENTS { get; set; }
        public System.Nullable<bool> FLAG { get; set; }
        public int? ACT_TP { get; set; }
        public System.DateTime? ACT_DT { get; set; }
    }

}
