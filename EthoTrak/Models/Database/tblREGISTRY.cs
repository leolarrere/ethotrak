using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class tblREGISTRY {
        public tblREGISTRY() { }
        public int RegistryID { get; set; }
        public string RegistryName { get; set; }
        public string RegistryValue { get; set; }
        public System.DateTime?DateCreated { get; set; }
        public System.DateTime?DateLastChanged { get; set; }
    }
}
