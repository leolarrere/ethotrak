using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblASSIGNED_HOBIDMap : ClassMap<TblASSIGNED_HOBID> {
        
        public TblASSIGNED_HOBIDMap() {
			Table("TblASSIGNED_HOBIDS");
			LazyLoad();
			CompositeId();
			Map(x => x.ASGN_HOB_ID).Column("ASGN_HOB_ID").Not.Nullable();
			Map(x => x.SPEC_ID).Column("SPEC_ID");
			Map(x => x.BEHAV_ID).Column("BEHAV_ID");
        }
    }
}
