using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class qryExhibitSpecy {
        public qryExhibitSpecy() { }
        public int Exhib_ID { get; set; }
        public int SPEC_ID { get; set; }
        public string SPEC_NAME { get; set; }
        public string LATIN_NAME { get; set; }
        public int? SPEC_PRNT { get; set; }
        public string HOBIDS { get; set; }
        public int? ACT_TP { get; set; }
        public System.DateTime?ACT_DT { get; set; }
    }
}
