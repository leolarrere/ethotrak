using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblEXHIB_IDMap : ClassMap<TblEXHIB_ID> {
        
        public TblEXHIB_IDMap() {
			Table("TblEXHIB_ID");
			LazyLoad();
			Id(x => x.EXHIB_ID).GeneratedBy.Identity().Column("EXHIB_ID");
			Map(x => x.EXHIB).Column("EXHIB").Length(50);
			Map(x => x.ADD_DATE).Column("ADD_DATE");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
