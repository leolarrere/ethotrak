using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblNEIGH_OBS_cMap : ClassMap<TblNEIGH_OBS_c> {
        
        public TblNEIGH_OBS_cMap() {
			Table("TblNEIGH_OBS_c");
			LazyLoad();
			CompositeId().KeyProperty(x => x.USER_ID, "USER_ID").KeyProperty(x => x.SES_NO, "SES_NO").KeyProperty(x => x.OBS_NO, "OBS_NO").KeyProperty(x => x.NE_ORD, "NE_ORD");
			Map(x => x.ID).Column("ID").Not.Nullable();
			Map(x => x.ID_OBS).Column("ID_OBS");
			Map(x => x.SPEC_ID).Column("SPEC_ID");
			Map(x => x.NE_OBS).Column("NE_OBS");
			Map(x => x.RELAT).Column("RELAT").Length(50);
			Map(x => x.DIST).Column("DIST");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
