using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class qryExhibitBehaviorMap : ClassMap<qryExhibitBehavior> {
        
        public qryExhibitBehaviorMap() {
			Table("qryExhibitBehaviors");
			LazyLoad();
			CompositeId();
			Map(x => x.Exhib_ID).Column("Exhib_ID").Not.Nullable();
			Map(x => x.BEHAV_ID).Column("BEHAV_ID").Not.Nullable();
			Map(x => x.MSTR_ID).Column("MSTR_ID");
			Map(x => x.SPEC_ID).Column("SPEC_ID");
			Map(x => x.BEHAV).Column("BEHAV").Length(50);
			Map(x => x.DEFIN).Column("DEFIN").Length(382);
			Map(x => x.ALL_OCC).Column("ALL_OCC");
			Map(x => x.ADD_DATE).Column("ADD_DATE");
			Map(x => x.BEH_PRNT).Column("BEH_PRNT");
			Map(x => x.Locked).Column("Locked");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
