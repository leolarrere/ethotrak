using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class sysdiagramMap : ClassMap<sysdiagram> {
        
        public sysdiagramMap() {
			Table("sysdiagrams");
			LazyLoad();
			Id(x => x.diagram_id).GeneratedBy.Identity().Column("diagram_id");
			Map(x => x.name).Column("name").Not.Nullable().Length(128);
			Map(x => x.principal_id).Column("principal_id").Not.Nullable();
			Map(x => x.version).Column("version");
			Map(x => x.definition).Column("definition");
        }
    }
}
