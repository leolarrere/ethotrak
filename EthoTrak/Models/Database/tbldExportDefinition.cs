using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class tbldExportDefinition {
        public tbldExportDefinition() { }
        public string ExportID { get; set; }
        public string ExportName { get; set; }
        public string ExportDesc { get; set; }
        public string Source { get; set; }
        public string SourceFilter { get; set; }
        public string RollBackFilter { get; set; }
        public string AffectedTable { get; set; }
        public string PrimaryIDName { get; set; }
        public string AffectedColumns { get; set; }
        public int? ActionCode { get; set; }
    }
}
