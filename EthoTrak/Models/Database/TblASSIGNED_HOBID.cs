using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblASSIGNED_HOBID {
        public TblASSIGNED_HOBID() { }
        public int ASGN_HOB_ID { get; set; }
        public int? SPEC_ID { get; set; }
        public int? BEHAV_ID { get; set; }
    }
}
