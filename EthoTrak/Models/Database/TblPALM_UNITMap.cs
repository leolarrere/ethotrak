using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblPALM_UNITMap : ClassMap<TblPALM_UNIT> {
        
        public TblPALM_UNITMap() {
			Table("TblPALM_UNIT");
			LazyLoad();
			Id(x => x.PALM_NO).GeneratedBy.Identity().Column("PALM_NO");
			Map(x => x.PALM_ID).Column("PALM_ID").Length(50);
			Map(x => x.SER_NO).Column("SER_NO").Length(50);
			Map(x => x.MODEL).Column("MODEL").Length(50);
			Map(x => x.USB).Column("USB");
			Map(x => x.OS).Column("OS").Length(50);
			Map(x => x.NOTES).Column("NOTES").Length(1073741823);
        }
    }
}
