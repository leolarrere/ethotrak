using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class tblREGISTRYMap : ClassMap<tblREGISTRY> {
        
        public tblREGISTRYMap() {
			Table("tblREGISTRY");
			LazyLoad();
			Id(x => x.RegistryID).GeneratedBy.Identity().Column("RegistryID");
			Map(x => x.RegistryName).Column("RegistryName").Length(20);
			Map(x => x.RegistryValue).Column("RegistryValue").Length(255);
			Map(x => x.DateCreated).Column("DateCreated");
			Map(x => x.DateLastChanged).Column("DateLastChanged");
        }
    }
}
