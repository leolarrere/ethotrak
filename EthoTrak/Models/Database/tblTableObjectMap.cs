using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class tblTableObjectMap : ClassMap<tblTableObject> {
        
        public tblTableObjectMap() {
			Table("tblTableObjects");
			LazyLoad();
			Id(x => x.ID).GeneratedBy.Identity().Column("ID");
			Map(x => x.TableName).Column("TableName").Length(50);
			Map(x => x.Description).Column("Description").Length(50);
			Map(x => x.DatabaseName).Column("DatabaseName").Length(50);
			Map(x => x.SourceTable).Column("SourceTable").Length(50);
			Map(x => x.TableType).Column("TableType");
        }
    }
}
