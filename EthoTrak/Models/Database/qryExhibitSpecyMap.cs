using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class qryExhibitSpecyMap : ClassMap<qryExhibitSpecy> {
        
        public qryExhibitSpecyMap() {
			Table("qryExhibitSpecies");
			LazyLoad();
			CompositeId();
			Map(x => x.Exhib_ID).Column("Exhib_ID").Not.Nullable();
			Map(x => x.SPEC_ID).Column("SPEC_ID").Not.Nullable();
			Map(x => x.SPEC_NAME).Column("SPEC_NAME").Length(50);
			Map(x => x.LATIN_NAME).Column("LATIN_NAME").Length(50);
			Map(x => x.SPEC_PRNT).Column("SPEC_PRNT");
			Map(x => x.HOBIDS).Column("HOBIDS").Length(50);
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
