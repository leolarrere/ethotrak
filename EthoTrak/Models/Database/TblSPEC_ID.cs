using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblSPEC_ID {
        public TblSPEC_ID() { }
        public virtual int SPEC_ID { get; set; }
        public virtual string SPEC_NAME { get; set; }
        public virtual string LATIN_NAME { get; set; }
        public virtual string COMMENTS { get; set; }
        public virtual int? SPEC_PRNT { get; set; }
        public virtual string HOBIDS { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
