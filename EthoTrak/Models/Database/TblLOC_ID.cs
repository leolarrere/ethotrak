using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblLOC_ID {
        public TblLOC_ID() { }
        public virtual int LOC_ID { get; set; }
        public virtual int? ENCLOS_ID { get; set; }
        public virtual string LOC { get; set; }
        public virtual System.DateTime? ADD_DATE { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
