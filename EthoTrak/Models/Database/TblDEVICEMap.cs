using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblDEVICEMap : ClassMap<TblDEVICE> {
        
        public TblDEVICEMap() {
			Table("TblDEVICE");
			LazyLoad();
			CompositeId();
			Map(x => x.DevID).Column("DevID").Not.Nullable();
			Map(x => x.USER_ID).Column("USER_ID").Length(8);
			Map(x => x.DateCreated).Column("DateCreated");
        }
    }
}
