using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblSPEC_IDMap : ClassMap<TblSPEC_ID> {
        
        public TblSPEC_IDMap() {
			Table("TblSPEC_ID");
			LazyLoad();
			Id(x => x.SPEC_ID).GeneratedBy.Identity().Column("SPEC_ID");
			Map(x => x.SPEC_NAME).Column("SPEC_NAME").Length(50);
			Map(x => x.LATIN_NAME).Column("LATIN_NAME").Length(50);
			Map(x => x.COMMENTS).Column("COMMENTS").Length(1073741823);
			Map(x => x.SPEC_PRNT).Column("SPEC_PRNT");
			Map(x => x.HOBIDS).Column("HOBIDS").Length(50);
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
