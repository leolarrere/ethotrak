﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class KeeperAssignmentController : Controller
    {

        public ActionResult PickExhibit()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var exhibits = session.QueryOver<TblEXHIB_ID>().List();

                    IEnumerable<SelectListItem> list = exhibits.Select(exhibit => new SelectListItem
                    {
                        Value = exhibit.EXHIB_ID.ToString(),
                        Text = exhibit.EXHIB
                    });

                    ViewBag.exhibits = list;
                }
            }
            return View();
        }

        public ActionResult ShowKeepers(int idExhibit)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var keepersAlreadyAssigned = session.QueryOver<TblEXHIB_KEEP>().Where(x => x.EXHIB_ID == idExhibit).Select(x => x.KEEP_ID).List<int>().ToArray();

                    var keepersNotAssigned = session.QueryOver<TblKEEP_ID>().WhereRestrictionOn(x => x.KEEP_ID).Not.IsIn(keepersAlreadyAssigned).Select(x => x.KEEP_ID).List<int>();

                    List<TblKEEP_ID> keepers = new List<TblKEEP_ID>();

                    foreach (var keeper in keepersNotAssigned)
                    {
                        keepers.Add(session.QueryOver<TblKEEP_ID>().Where(x => x.KEEP_ID == Convert.ToInt32(keeper)).SingleOrDefault());
                    }
                    ViewBag.keepers = keepers;
                    ViewBag.idExhibit = idExhibit;
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Assign(int idExhibit, List<int> keepers)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (int id in keepers)
                    {
                        var assignment = new TblEXHIB_KEEP();

                        assignment.KEEP_ID = id;
                        assignment.EXHIB_ID = idExhibit;
                        assignment.ADD_DATE = System.DateTime.Now;
                        assignment.ACT_DT = assignment.ADD_DATE;

                        session.Save(assignment);
                    }
                    transaction.Commit();
                }
            }
            return Redirect("/KeeperAssignment/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.assignments = session.QueryOver<TblEXHIB_KEEP>().List().OrderBy(x => x.REC_ID);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbAssignment = session.QueryOver<TblEXHIB_KEEP>().Where(x => x.REC_ID == id).SingleOrDefault();
                    session.Delete(dbAssignment);
                    transaction.Commit();
                }
            }
            return Redirect("/KeeperAssignment/All");
        }

    }
}
