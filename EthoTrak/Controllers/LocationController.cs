﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class LocationController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Add()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var enclosures = session.QueryOver<TblENCLOS_ID>().List();

                    IEnumerable<SelectListItem> list = enclosures.Select(e => new SelectListItem
                    {
                        Value = e.ENCLOS_ID.ToString(),
                        Text = e.ENCLOS
                    });

                    ViewBag.enclosures = list;
                }
            }

            var location = new TblLOC_ID();
            return View(location);
        }

        [HttpPost]
        public ActionResult Add(TblLOC_ID location)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    location.ADD_DATE = System.DateTime.Now;
                    location.ACT_DT = location.ADD_DATE;

                    session.Save(location);

                    transaction.Commit();
                }
            }

            return Redirect("/Location/All");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var location = session.QueryOver<TblLOC_ID>().Where(x => x.LOC_ID == id).SingleOrDefault();

                    if (location == null)
                    {
                        return HttpNotFound();
                    }

                    var enclosures = session.QueryOver<TblENCLOS_ID>().List();

                    IEnumerable<SelectListItem> list = enclosures.Select(e => new SelectListItem
                    {
                        Value = e.ENCLOS_ID.ToString(),
                        Text = e.ENCLOS
                    });

                    ViewBag.enclosures = list;

                    return View(location);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblLOC_ID location)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbLocation = session.QueryOver<TblLOC_ID>().Where(x => x.LOC_ID == id).SingleOrDefault();

                    dbLocation.LOC = location.LOC;
                    dbLocation.ENCLOS_ID = location.ENCLOS_ID;
                    dbLocation.ACT_DT = System.DateTime.Now;

                    session.Save(dbLocation);

                    transaction.Commit();
                }
            }
            return Redirect("/Location/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.locations = session.QueryOver<TblLOC_ID>().List().OrderBy(x => x.LOC_ID);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbLocation = session.QueryOver<TblLOC_ID>().Where(x => x.LOC_ID == id).SingleOrDefault();
                    session.Delete(dbLocation);
                    transaction.Commit();
                }
            }
            return Redirect("/Location/All");
        }
    }
}
