﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class EnclosureController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Add()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var areas = session.QueryOver<TblAREA_ID>().List();

                    IEnumerable<SelectListItem> list = areas.Select(a => new SelectListItem
                    {
                        Value = a.AREA_ID.ToString(),
                        Text = a.AREA
                    });

                    ViewBag.areas = list;
                }
            }

            var enclosure = new TblENCLOS_ID();
            return View(enclosure);
        }

        [HttpPost]
        public ActionResult Add(TblENCLOS_ID enclosure)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    enclosure.ACT_DT = System.DateTime.Now;

                    session.Save(enclosure);

                    transaction.Commit();
                }
            }

            return Redirect("/Enclosure/All");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var enclosure = session.QueryOver<TblENCLOS_ID>().Where(x => x.ENCLOS_ID == id).SingleOrDefault();

                    if (enclosure == null)
                    {
                        return HttpNotFound();
                    }

                    var areas = session.QueryOver<TblAREA_ID>().List();

                    IEnumerable<SelectListItem> list = areas.Select(a => new SelectListItem
                    {
                        Value = a.AREA_ID.ToString(),
                        Text = a.AREA
                    });

                    ViewBag.areas = list;

                    return View(enclosure);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblENCLOS_ID enclosure)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbEnclosure = session.QueryOver<TblENCLOS_ID>().Where(x => x.ENCLOS_ID == id).SingleOrDefault();

                    dbEnclosure.ENCLOS = enclosure.ENCLOS;
                    dbEnclosure.AREA_ID = enclosure.AREA_ID;
                    dbEnclosure.COMMENTS = enclosure.COMMENTS;
                    dbEnclosure.ACT_DT = System.DateTime.Now;

                    session.Save(dbEnclosure);

                    transaction.Commit();
                }
            }
            return Redirect("/Enclosure/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.enclosures = session.QueryOver<TblENCLOS_ID>().List().OrderBy(x => x.ENCLOS_ID);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbLocations = session.QueryOver<TblLOC_ID>().Where(x => x.ENCLOS_ID == id).List();

                    foreach (var location in dbLocations)
                    {
                        EthoTrak.Controllers.LocationController lc = new LocationController();
                        lc.Delete(location.LOC_ID);
                    }

                    var dbEnclosure = session.QueryOver<TblENCLOS_ID>().Where(x => x.ENCLOS_ID == id).SingleOrDefault();
                    session.Delete(dbEnclosure);
                    transaction.Commit();
                }
            }
            return Redirect("/Enclosure/All");
        }
    }
}
