﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class KeeperController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Add()
        {
            var keeper = new TblKEEP_ID();
            return View(keeper);
        }

        [HttpPost]
        public ActionResult Add(TblKEEP_ID keeper)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    keeper.ACT_DT = System.DateTime.Now;
                    keeper.ADD_DATE = keeper.ACT_DT;

                    session.Save(keeper);
                    transaction.Commit();
                }
            }
            return Redirect("/Keeper/All");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var keeper = session.QueryOver<TblKEEP_ID>().Where(x => x.KEEP_ID == id).SingleOrDefault();

                    if (keeper == null)
                    {
                        return HttpNotFound();
                    }

                    ViewBag.Date = keeper.ADD_DATE.Value.ToShortDateString();

                    return View(keeper);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblKEEP_ID keeper)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbKeeper = session.QueryOver<TblKEEP_ID>().Where(x => x.KEEP_ID == id).SingleOrDefault();

                    dbKeeper.FIRST = keeper.FIRST;
                    dbKeeper.MID_INIT = keeper.MID_INIT;
                    dbKeeper.LAST = keeper.LAST;
                    dbKeeper.ADD_DATE = keeper.ADD_DATE;
                    dbKeeper.ACT_DT = System.DateTime.Now;

                    session.Save(dbKeeper);

                    transaction.Commit();
                }
            }
            return Redirect("/Keeper/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.keepers = session.QueryOver<TblKEEP_ID>().List().OrderBy(x => x.KEEP_ID);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbKeeper = session.QueryOver<TblKEEP_ID>().Where(x => x.KEEP_ID == id).SingleOrDefault();
                    session.Delete(dbKeeper);
                    transaction.Commit();
                }
            }
            return Redirect("/Keeper/All");
        }
    }
}
