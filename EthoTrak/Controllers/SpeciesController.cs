﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    
    public class SpeciesController : Controller
    {
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult Add()
        {
            var species = new TblSPEC_ID();
            return View(species);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult Add(TblSPEC_ID species)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    species.ACT_DT = System.DateTime.Now;

                    session.Save(species);
                    transaction.Commit();                    
                }
            }
            return Redirect("/Species/AllAdmin");
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var species = session.QueryOver<TblSPEC_ID>().Where(x => x.SPEC_ID == id).SingleOrDefault();

                    if (species == null)
                    {
                        return HttpNotFound();
                    }

                    return View(species);
                }
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id, TblSPEC_ID species)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbSpecies = session.QueryOver<TblSPEC_ID>().Where(x => x.SPEC_ID == id).SingleOrDefault();

                    dbSpecies.SPEC_NAME = species.SPEC_NAME;
                    dbSpecies.LATIN_NAME = species.LATIN_NAME;
                    dbSpecies.COMMENTS = species.COMMENTS;
                    dbSpecies.ACT_DT = System.DateTime.Now;

                    session.Save(dbSpecies);

                    transaction.Commit();
                }
            }
            return Redirect("/Species/AllAdmin");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult AllAdmin()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.species = session.QueryOver<TblSPEC_ID>().List().OrderBy(x => x.SPEC_ID);
                }
            }

            return View();
        }

        [Authorize(Roles = "Free, Paid Low, Paid High, Administrator")]
        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.species = session.QueryOver<TblSPEC_ID>().List().OrderBy(x => x.SPEC_ID);
                }
            }

            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbSpecies = session.QueryOver<TblSPEC_ID>().Where(x => x.SPEC_ID == id).SingleOrDefault();
                    session.Delete(dbSpecies);
                    transaction.Commit();
                }
            }
            return Redirect("/Species/AllAdmin");
        }
    }
}
