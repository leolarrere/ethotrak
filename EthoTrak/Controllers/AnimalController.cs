﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class AnimalController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Add()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var species = session.QueryOver<TblSPEC_ID>().List();

                    IEnumerable<SelectListItem> list = species.Select(s => new SelectListItem
                    {
                        Value = s.SPEC_ID.ToString(),
                        Text = s.SPEC_NAME
                    });

                    ViewBag.species = list;
                }
            }
            var animal = new TblANIM_ID();
            return View(animal);
        }

        [HttpPost]
        public ActionResult Add(TblANIM_ID animal)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    animal.ACT_DT = System.DateTime.Now;

                    if (ModelState.IsValid)
                    {
                        session.Save(animal);
                        transaction.Commit();
                    }
                    else
                    {                        
                        var species = session.QueryOver<TblSPEC_ID>().List();

                        IEnumerable<SelectListItem> list = species.Select(s => new SelectListItem
                        {
                            Value = s.SPEC_ID.ToString(),
                            Text = s.SPEC_NAME
                        });

                        ViewBag.species = list;
                        return View(animal);
                    }
                }
            }

            return Redirect("/Animal/All");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var animal = session.QueryOver<TblANIM_ID>().Where(x => x.ANIM_ID == id).SingleOrDefault();

                    if (animal == null)
                    {
                        return HttpNotFound();
                    }

                    var species = session.QueryOver<TblSPEC_ID>().List();

                    IEnumerable<SelectListItem> list = species.Select(s => new SelectListItem
                    {
                        Value = s.SPEC_ID.ToString(),
                        Text = s.SPEC_NAME
                    });

                    ViewBag.species = list;

                    return View(animal);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblANIM_ID animal)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbAnimal = session.QueryOver<TblANIM_ID>().Where(x => x.ANIM_ID == id).SingleOrDefault();

                    dbAnimal.DESC_ANIM = animal.DESC_ANIM;
                    dbAnimal.SPEC_ID = animal.SPEC_ID;
                    dbAnimal.INST_ID = animal.INST_ID;
                    dbAnimal.SEX = animal.SEX;
                    dbAnimal.STATUS = animal.STATUS;
                    dbAnimal.COMMENTS = animal.COMMENTS;
                    dbAnimal.ACT_DT = System.DateTime.Now;

                    session.Save(dbAnimal);

                    transaction.Commit();
                }
            }
            return Redirect("/Animal/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.animals = session.QueryOver<TblANIM_ID>().List().OrderBy(x => x.ANIM_ID);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbAnimal = session.QueryOver<TblANIM_ID>().Where(x => x.ANIM_ID == id).SingleOrDefault();
                    session.Delete(dbAnimal);
                    transaction.Commit();
                }
            }
            return Redirect("/Animal/All");
        }
    }
}
