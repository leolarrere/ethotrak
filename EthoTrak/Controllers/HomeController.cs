﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.Models;

namespace EthoTrak.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "This is the index.";
            
            return View();
        }

        public ActionResult Management()
        {
            return View();
        }
    }
}
