﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    //[Authorize(Roles = "Free, Paid Low, Paid High, Administrator")]
    public class EnclosureAssignmentController : Controller
    { 
        public ActionResult PickExhibit()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var exhibits = session.QueryOver<TblEXHIB_ID>().List();

                    IEnumerable<SelectListItem> list = exhibits.Select(exhibit => new SelectListItem
                    {
                        Value = exhibit.EXHIB_ID.ToString(),
                        Text = exhibit.EXHIB
                    });

                    ViewBag.exhibits = list;
                }
            }
            return View();
        }

        public ActionResult PickArea(int idExhibit)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var areas = session.QueryOver<TblAREA_ID>().Where(x => x.EXHIB_ID == idExhibit).List();

                    IEnumerable<SelectListItem> list = areas.Select(a => new SelectListItem
                    {
                        Value = a.AREA_ID.ToString(),
                        Text = a.AREA
                    });

                    ViewBag.areas = list;
                }
            }
            return View();
        }

        public ActionResult PickEnclosure(int idArea)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var enclosures = session.QueryOver<TblENCLOS_ID>().Where(x => x.AREA_ID == idArea).List();

                    IEnumerable<SelectListItem> list = enclosures.Select(e => new SelectListItem
                    {
                        Value = e.ENCLOS_ID.ToString(),
                        Text = e.ENCLOS
                    });

                    ViewBag.enclosures = list;
                }
            }
            return View();
        }

        public ActionResult ShowAnimals(int idEnclosure)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var animalsAlreadyInEnclosure = session.QueryOver<TblANIM_ENCLO>().Where(x => x.ENCLOS_ID == idEnclosure).Select(x => x.ANIM_ID).List<int>().ToArray();

                    var animalsNotInEnclosure = session.QueryOver<TblANIM_ID>().WhereRestrictionOn(x => x.ANIM_ID).Not.IsIn(animalsAlreadyInEnclosure).Select(x => x.ANIM_ID).List<int>();

                    List<TblANIM_ID> animals = new List<TblANIM_ID>();

                    foreach (var animal in animalsNotInEnclosure)
                    {
                        animals.Add(session.QueryOver<TblANIM_ID>().Where(x => x.ANIM_ID == Convert.ToInt32(animal)).SingleOrDefault());
                    }
                    ViewBag.animals = animals;
                    ViewBag.idEnclosure = idEnclosure;
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Process(int idEnclosure, List<int> animals)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (int id in animals)
                    {
                        //System.Diagnostics.Debug.WriteLine(id);
                        var assignment = new TblANIM_ENCLO();

                        assignment.ANIM_ID = id;
                        assignment.ENCLOS_ID = idEnclosure;
                        assignment.IN_DATE = System.DateTime.Now;
                        assignment.ACT_DT = assignment.IN_DATE;

                        session.Save(assignment);
                    }
                    transaction.Commit();
                }
            }
            return Redirect("/EnclosureAssignment/All");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var assignment = session.QueryOver<TblANIM_ENCLO>().Where(x => x.ENCLOS_REC == id).SingleOrDefault();

                    if (assignment == null)
                    {
                        return HttpNotFound();
                    }

                    ViewBag.inDate = assignment.IN_DATE.Value.ToShortDateString();
                    if (assignment.OUT_DATE.HasValue)
                        ViewBag.outDate = assignment.OUT_DATE.Value.ToShortDateString();

                    return View(assignment);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblANIM_ENCLO assignment)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbAssignment = session.QueryOver<TblANIM_ENCLO>().Where(x => x.ENCLOS_REC == id).SingleOrDefault();

                    dbAssignment.IN_DATE = System.DateTime.Now;
                    dbAssignment.OUT_DATE = assignment.OUT_DATE;
                    dbAssignment.COMMENTS = assignment.COMMENTS;
                    dbAssignment.ACT_DT = System.DateTime.Now;

                    session.Save(dbAssignment);

                    transaction.Commit();
                }
            }
            return Redirect("/EnclosureAssignment/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.assignments = session.QueryOver<TblANIM_ENCLO>().List().OrderBy(x => x.ENCLOS_REC);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbAssignment = session.QueryOver<TblANIM_ENCLO>().Where(x => x.ENCLOS_REC == id).SingleOrDefault();
                    session.Delete(dbAssignment);
                    transaction.Commit();
                }
            }
            return Redirect("/EnclosureAssignment/All");
        }

    }
}
